package plt;

public class Translator {
	
	public final static String NIL = "nil";
	
	private String phrase;

	public Translator(String inputPhrase) {
		phrase = inputPhrase;
	}

	public String getPhrase() {
		return phrase;
	}

	public String translate() {
		if( !phrase.isBlank()) {
			if( phrase.indexOf(" ") != -1 ) {
				dividePhrasesAndAssembleItTranslatedForEachWord(" ");
				return phrase;
			}else if( phrase.indexOf("-") != -1 ) {
				dividePhrasesAndAssembleItTranslatedForEachWord("-");
				return phrase;
			}else {
				if(startsWithVowel()) {
					if( phrase.endsWith("y") ) {
						return phrase + "nay";
					} else if(endsWithVowel()) {
						return phrase + "yay";
					} else {
						return phrase + "ay";
					}
				}else {
					
					if( phraseStartingConsonants() > 1) {
						return phrase.substring( phraseStartingConsonants(), phrase.length()) + phrase.substring(0, phraseStartingConsonants() ) + "ay";
					}else {
						return phrase.substring(1, phrase.length()) + phrase.charAt(0) + "ay";
					}
					
				}
			}
			
		}else {
			return NIL;
		}

	}
	
	
	private boolean checkPunctation() {
		return ".,:;!?'()".indexOf(phrase.charAt(phrase.length()-1)) != -1;
	}
	
	private void dividePhrasesAndAssembleItTranslatedForEachWord(String toBeSplittetWith) {
		char punctationFound = Character.MIN_VALUE;
		String[] phraseDividedByHyphens = phrase.split(toBeSplittetWith);
		for (int i = 0; i < phraseDividedByHyphens.length; i++) {
			phrase = phraseDividedByHyphens[i];
			if( checkPunctation()) {
				punctationFound = phrase.charAt(phrase.length()-1);
				phrase = phrase.substring(0, phrase.length()-1);
			}
			phraseDividedByHyphens[i] = translate();
		}
		assembleTranslatedStringArray(phraseDividedByHyphens, toBeSplittetWith, punctationFound);
	}

	private void assembleTranslatedStringArray(String[] phraseDividedByHyphens, String divisor, char punctationFound) {
		
		StringBuffer stringBuffer = new StringBuffer();
		for (int i = 0; i < phraseDividedByHyphens.length; i++) {
			stringBuffer.append(phraseDividedByHyphens[i]);
			if( i < phraseDividedByHyphens.length-1) {
				stringBuffer.append(divisor);
			}
		}
		if( punctationFound != Character.MIN_VALUE) {
			stringBuffer.append(punctationFound);
		}
		phrase = stringBuffer.toString();
		
	}

	private int phraseStartingConsonants() {
		int numberOfConsonantsFromStartInPhraseTillAVowel = 0;
		for (int i = 0; i < phrase.length(); i++) { 
			if( "aeiou".indexOf(phrase.charAt(i)) == -1 ) {
				numberOfConsonantsFromStartInPhraseTillAVowel++;
			}else {
				break;
			}
		}
		return numberOfConsonantsFromStartInPhraseTillAVowel;
	}
	
	private boolean startsWithVowel() {
		return phrase.startsWith("a") || phrase.startsWith("e") || phrase.startsWith("i") || phrase.startsWith("o") || phrase.startsWith("u"); 	
	}
	
	private boolean endsWithVowel() {
		return phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || phrase.endsWith("o") || phrase.endsWith("u"); 	
	}
	
}
